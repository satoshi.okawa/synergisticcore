## Installation
```bash
git clone https://git-r3lab.uni.lu/satoshi.okawa/synergisticcore path/to/workdir
cd path/to/workdir
```

## Usage
To run SynergisticCore.R for subpoulation "Oligo" in the sample dataset,
```bash
Rscript SynergisticCore.R  sample_data_Gokce2016.csv  Oligo
```

